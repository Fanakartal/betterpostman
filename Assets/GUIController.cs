﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GUIController : MonoBehaviour {

    public void GetNextLevel(int nextScene)
    {
        SceneManager.LoadScene(nextScene);
        //Application.LoadLevel(nextScene);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //Application.LoadLevel(Application.loadedLevel);
        //Debug.Log(Application.loadedLevel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
