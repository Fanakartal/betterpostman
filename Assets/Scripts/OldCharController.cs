﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OldCharController : MonoBehaviour {
    public static float score;
    public GameObject magneticBar;
    private float magneticBarValue;
    public static bool pause;

    private AudioSource backSource;
    public AudioClip backClip;

    float maxSpeed = 6.5f;
    float jumpForce = 1000f;
    float groundRadius = 0.2f;
    float timeToFly = 2.0f;
    float timePassed;

    //int collidedWithEnemy;

    //bool facingRight = true;
    bool grounded;

    public Transform groundCheck;
    public Texture2D image;
    public LayerMask whatIsGround;
    public GUIStyle guiStyle = new GUIStyle();

    //public GUIText textYouWon;

    // Use this for initialization
    void Start()
    {
        //magneticBarValue = magneticBar.GetComponent<Image>().fillAmount;
        score = 0.0f;
        //collidedWithEnemy = 0;
        Debug.Log("Time is set.");
        Time.timeScale = 1;
        timePassed = 0.0f;

        backSource = gameObject.AddComponent<AudioSource>();
        backSource.GetComponent<AudioSource>().clip = backClip;

        //textYouWon.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Time.timeScale = 1;
        //if (collidedWithEnemy == 0)
        //{

        /* Belli bir süre sonra manyetizma bittiği için aşağı düşmemizi sağlayan parçacık */
        if (grounded && GetComponent<Rigidbody2D>().gravityScale == -1)
        {
            timePassed = timePassed + Time.deltaTime;
            //magneticBar.GetComponent<Image>().fillAmount -= (timePassed / 100);
            Debug.Log(timePassed);

            if (timePassed >= timeToFly)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -jumpForce));
                Flip();
                GetComponent<Rigidbody2D>().gravityScale *= -1;
            }
        }

        /* Yerdeyken manyetizma çubuğunun bir anda değil süreyle dolmasını sağlayan parçacık */
        if (grounded && GetComponent<Rigidbody2D>().gravityScale == 1)
        {
            if (timePassed > 0.0f * timeToFly)
            {
                Debug.Log(timePassed);
                timePassed = timePassed - Time.deltaTime;
                //magneticBar.GetComponent<Image>().fillAmount += (timePassed / 100);
            }
        }

        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            if (GetComponent<Rigidbody2D>().gravityScale == 1)
            {
                // Manyetizma çubuğu tam dolmadan zıplanması istenmiyorsa bu açılır.
                //if (timePassed < 0.03f)
                //{
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
                Flip();
                GetComponent<Rigidbody2D>().gravityScale *= -1;
                //}
            }
            else if (GetComponent<Rigidbody2D>().gravityScale == -1)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -jumpForce));
                Flip();
                GetComponent<Rigidbody2D>().gravityScale *= -1;
            }
        }
        //}
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        GetComponent<Rigidbody2D>().velocity = new Vector2(/*move * */ maxSpeed, GetComponent<Rigidbody2D>().velocity.y);


        /*float move = -(transform.position.x);*/
        //Input.GetAxis("Horizontal");

        //if (move > 0 && !facingRight) Flip();
        //if (Input.GetKeyDown(KeyCode.Space)) Flip();
        //else if (move < 0 && facingRight) Flip();    
    }

    void OnGUI()
    {
        guiStyle.fontStyle = FontStyle.Bold;
        guiStyle.fontSize = 30;

        if (timePassed > 0.0f && timePassed < 0.7f) //  || timeGround > 1.4f && timeGround < 2.05f
        {
            GUI.Box(new Rect(Screen.width - 210, 0, 180, 50), "");
            GUI.Label(new Rect(Screen.width - 210, 60, 180, 50), "Magnetism", guiStyle);
            
            GUI.Label(new Rect(Screen.width - 150, 0, 50, 50), image);
            GUI.Label(new Rect(Screen.width - 190, 0, 50, 50), image);
        }
        else if (timePassed > 0.7f && timePassed < 1.4f) // || timeGround > 0.7f && timeGround < 1.4f
        {
            GUI.Box(new Rect(Screen.width - 210, 0, 180, 50), "");
            GUI.Label(new Rect(Screen.width - 210, 60, 180, 50), "Magnetism", guiStyle);
            
            GUI.Label(new Rect(Screen.width - 190, 0, 50, 50), image);
        }
        else if (timePassed > 1.4f && timePassed < 2.05f) // || timeGround > 0.2f && timeGround < 0.7f
        {
            GUI.Box(new Rect(Screen.width - 210, 0, 180, 50), "");
            GUI.Label(new Rect(Screen.width - 210, 60, 180, 50), "Magnetism", guiStyle);
        }

        else
        {
            GUI.Box(new Rect(Screen.width - 210, 0, 180, 50), "");
            GUI.Label(new Rect(Screen.width - 210, 60, 180, 50), "Magnetism", guiStyle);
            
            GUI.Label(new Rect(Screen.width - 110, 0, 50, 50), image);
            GUI.Label(new Rect(Screen.width - 150, 0, 50, 50), image);
            GUI.Label(new Rect(Screen.width - 190, 0, 50, 50), image);            
        }
    }

    void Flip()
    {
        //transform.rotation *= Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + 180);
        //facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.y = theScale.y * -1;
        transform.localScale = theScale;

    }

    // TODO: Sağdan ya da soldan yani hangi yönden collision olduğu tespit edildiğinde bunu çalıştırabiliriz.

    /*void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "DeadEnd" || other.gameObject.tag == "Dog" || other.gameObject.tag == "Dave")
        {
            collidedWithEnemy = 1;
            Debug.Log("Collided with something");
        }
    }*/

    /*void OnTriggerEnter2D(Collider2D scorer)
    {
        if (scorer.gameObject.tag == "Envelope")
        {
            score += Random.Range(2, 5);
        }
    }*/

    IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "House")
        {
            //textYouWon.enabled = true;
            //print("Starting " + Time.time);
            yield return StartCoroutine(WaitABit(1.8f));
            //print("Before WaitAndPrint Finishes " + Time.time);
            //yield return StartCoroutine(StopEverything());

            //Application.LoadLevel(0);
            SceneManager.LoadScene(0);
        }

        if (other.gameObject.tag == "Envelope")
        {
            backSource.GetComponent<AudioSource>().Play();
            score += Random.Range(2, 5);
            Destroy(other.gameObject);
        }
    }

    IEnumerator WaitABit(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);

        //print("WaitAndPrint " + Time.time);
    }

    public void StopEverything()
    {
        //yield return new WaitForSeconds(0.0f);
        if (Time.timeScale > 0)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
        //pauseStatus = false;

        //print("Time has stopped.");
    }
}
