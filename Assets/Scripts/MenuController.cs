﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	Color initialColor;
    
    // Use this for initialization
    void Start () 
    {
        initialColor = GetComponent<Renderer>().material.color;
	}
	
    void OnMouseDown()
    {

        if (GetComponent<Collider2D>().gameObject.tag == "Turkey")
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            //Application.LoadLevel(Application.loadedLevel + 1);
        else if (GetComponent<Collider2D>().gameObject.tag == "Exit")
            Application.Quit();
        else if (SceneManager.GetActiveScene().buildIndex == 1)
        {

            if (GetComponent<Collider2D>().gameObject.tag == "Ankara")
                //Application.LoadLevel(2);
                SceneManager.LoadScene(2);
            if (GetComponent<Collider2D>().gameObject.tag == "Izmir")
                //Application.LoadLevel(2);
                SceneManager.LoadScene(2);
            if (GetComponent<Collider2D>().gameObject.tag == "Istanbul")
                //Application.LoadLevel(2);
                SceneManager.LoadScene(2);
            if (GetComponent<Collider2D>().gameObject.tag == "Secret")
            {
                Debug.Log("Secret level tıklandı.");
                //Application.LoadLevel(2);
                SceneManager.LoadScene(2);
            }
            if (GetComponent<Collider2D>().gameObject.tag == "Back")
                //Application.LoadLevel(0);
                SceneManager.LoadScene(0);
        }
    }

    void OnMouseOver()
    {
        if (GetComponent<Collider2D>().gameObject.tag == "Secret")
        {
            //Do nothing
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(initialColor, Color.cyan, 0.5f);
        }
    }

    void OnMouseExit()
    {
        if (GetComponent<Collider2D>().gameObject.tag == "Secret")
        {
            //Do nothing
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = initialColor;
        }
    }
}